import argparse

parser = argparse.ArgumentParser(description='reid')

parser.add_argument('--data_path',
                    default="Market-1501-v15.09.15",
                    help='path of Market-1501-v15.09.15')

parser.add_argument('--mode',
                    default='train', choices=['train', 'evaluate', 'vis'],
                    help='train or evaluate ')

parser.add_argument('--query_image',
                    default='0001_c1s1_001051_00.jpg',
                    help='path to the image you want to query')

parser.add_argument('--freeze',
                    default=False,
                    help='freeze backbone or not ')

parser.add_argument('--weight',
                    default='weights/model.pt',
                    help='load weights ')

parser.add_argument('--epoch', type=int,
                    default=100,
                    help='number of epoch to train')

parser.add_argument('--lr', type=float,
                    default=2e-4,
                    help='initial learning_rate')
                    
parser.add_argument('--model',
                    default='default',
                    help='Base Model')

parser.add_argument('--optimizer',
                    default='adam',
                    help='Base Model')

parser.add_argument('--lr_scheduler',
                    default=[40,80],
                    help='MultiStepLR,decay the learning rate')

parser.add_argument("--batchid",type=int, #untuk batch id yang mau ditraining
                    default=7,
                    help='the batch for id')

parser.add_argument("--batchimage",type=int,
                    default=4,
                    help='the batch of per id')

parser.add_argument("--batchtest",type=int,
                    default=8,
                    help='the batch size for test')

opt = parser.parse_args()
