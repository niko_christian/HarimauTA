import copy
import torch
import torch.nn as nn
from torchvision import models



num_classes = 107  # change this depend on your dataset


class Dense121(nn.Module):
    def __init__(self):
        super(Dense121, self).__init__()

        feats = 256
        self.model = models.densenet121(pretrained=True)
        self.maxpool = nn.AdaptiveAvgPool2d((1,1))
        self.fc_id = nn.Linear(1024, num_classes)

        self._init_fc(self.fc_id)
    
    @staticmethod
    def _init_fc(fc):
        nn.init.kaiming_normal_(fc.weight, mode='fan_out')
        # nn.init.normal_(fc.weight, std=0.001)
        nn.init.constant_(fc.bias, 0.)
        

    def forward(self, inputs):
        x = self.model.features(inputs)

        fg_p1 = self.maxpool(x).squeeze(dim=3).squeeze(dim=2)
#        print(fg_p1.size())
       
        l_p1  = self.fc_id(fg_p1)
      
       
        predict = torch.cat([fg_p1], dim=1)

        return predict, fg_p1, l_p1