

import os
import os.path as osp
import shutil
from PIL import Image
from tqdm import tqdm

path = './data/AmurTigerMulti/'

to_do_list = ['bounding_box_train','bounding_box_test']
to_do_more = ['Pseudo_Mask','Pseudo_MaskTEST']

def Third():
    for i in range(3):
        for n in to_do_list:
         
            # if not osp.exists(path + 'flod' + str(i) + '/' + n + '_filp'):
            #     os.mkdir(path + 'flod' + str(i) + '/' + n + '_filp')
            for pic in tqdm(os.listdir(path + 'flod' + str(i) + '/' + n)):
                im = Image.open(path + 'flod' + str(i) + '/' + n + '/' + pic)
                new_im = im.transpose(Image.FLIP_LEFT_RIGHT)
                name, ext = pic.split('.')   
                new_im.save(path + 'flod' + str(i) + '/' + 'query'+ '/' + name+ "_999.jpg") #yang flip diberi angka 999
                # if(n!='bounding_box_test'):
                #     im.save(path + 'flod' + str(i) + '/' + n + '_filp' + '/' + pic)
        # os.rename(path + 'flod' + str(i) + '/' +'bounding_box_test'+ '_filp',path + 'flod' + str(i) + '/' +'query')


        #Pseudo Mask
        os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'PseudoMaskQuery/')
        os.mkdir(path + 'flod' + str(i) + '/' + 'PseudoMaskQuery/'+'Body/')
        os.mkdir(path + 'flod' + str(i) + '/PseudoMaskQuery/'+'1part/')
        os.mkdir(path + 'flod' + str(i) + '/PseudoMaskQuery/'+'2part/')
        os.mkdir(path + 'flod' + str(i) + '/PseudoMaskQuery/'+'3part/')
        os.mkdir(path + 'flod' + str(i) + '/PseudoMaskQuery/'+'4part/')
        os.mkdir(path + 'flod' + str(i) + '/PseudoMaskQuery/'+'5part/')
        os.mkdir(path + 'flod' + str(i) + '/PseudoMaskQuery/'+'6part/')
        for n in to_do_more:
            # if not osp.exists(path + 'flod' + str(i) + '/' + n + '_filp'):
            #     os.mkdir(path + 'flod' + str(i) + '/' + n + '_filp')
            #     os.mkdir(path + 'flod' + str(i) + '/' + n + '_filp/'+'Body/')
            #     os.mkdir(path + 'flod' + str(i) + '/' + n + '_filp/'+'1part/')
            #     os.mkdir(path + 'flod' + str(i) + '/' + n + '_filp/'+'2part/')
            #     os.mkdir(path + 'flod' + str(i) + '/' + n + '_filp/'+'3part/')
            #     os.mkdir(path + 'flod' + str(i) + '/' + n + '_filp/'+'4part/')
            #     os.mkdir(path + 'flod' + str(i) + '/' + n + '_filp/'+'5part/')
            #     os.mkdir(path + 'flod' + str(i) + '/' + n + '_filp/'+'6part/')
       
            directory= path + 'flod' + str(i) + '/' + n
            for direc in os.listdir(directory):
                #print(i,n,direc)
                for pic in tqdm(os.listdir(path + 'flod' + str(i) + '/' + n + '/'+ direc+'/')):
                    im = Image.open(path + 'flod' + str(i) + '/' + n + '/'+direc+'/' + pic)
                    new_im = im.transpose(Image.FLIP_LEFT_RIGHT)
                    name,part,ext = pic.split('.')   
                    new_im.save(path + 'flod' + str(i) + '/' + 'PseudoMaskQuery'+ '/' + direc + '/' +name+'_999'+'.'+part+ ".jpg") #yang flip diberi angka 999
        #             if(n!='Pseudo_MaskTEST'):
        #                 im.save(path + 'flod' + str(i) + '/' + n + '_filp' + '/' + direc + '/'+ pic)
        
        # os.rename(path + 'flod' + str(i) + '/' +'Pseudo_MaskTEST'+ '_filp',path + 'flod' + str(i) + '/' +'PseudoMaskQuery')

