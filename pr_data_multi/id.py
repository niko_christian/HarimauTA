import os
import os.path as osp
import pandas as pd
import random
import shutil
from tqdm import tqdm
import json
import numpy as np




def First():
    #LOAD BAHAN
    id_path = './atrw_anno_reid_train/reid_list_train.csv'
    #pose_ann_path = './atrw_anno_reid_train/coba.json'

    pose_ann_path = './atrw_anno_reid_train/reid_keypoints_train.json'

    id_df = pd.read_csv(id_path, header=None, names=['id', 'img_file'])
    id_list = list(id_df['id'].unique())
    # print(len(id_list))     
    # print(id_list)     
    # print(len(id_df))     

    #cek kelengkapan body part
    lengkap=[]
    with open(pose_ann_path, 'r') as f:
                pos_json = json.load(f)
                for i in tqdm(pos_json):
                    pic_name = i
                    #print(pic_name)
                    pos = np.array(pos_json[i]).reshape((15, 3))
                    #print(pos)
                    value=True #dapat digunakan untuk training jika lengkap
                    for x in range(15):
                        if np.all(pos[x, 2] == 0):
                            value=False
                    if(value):
                        lengkap.append(pic_name)
                # print(lengkap)

    print(len(lengkap))

    #siapin data kosong 
    dict = {'id':[], 
            'img_file':[]
        } 
    idfilter = pd.DataFrame(dict) 
    for index, row in id_df.iterrows():
        for x in lengkap:
            if(row['img_file'] == x):
                idfilter.loc[len(idfilter.index)] = [row['id'],row['img_file']]  
                #print(row['id'],row['img_file'],x)
    print(idfilter)
    id_list = list(idfilter['id'].unique())
    print(id_list)
    print(len(id_list))

    random.shuffle(id_list)                     
    split_num = int(len(id_list)*0.2)           

    for i in range(3):
        start_split = i * split_num
        end_split = (i + 1) * split_num

        train_id_list = id_list[:start_split] + id_list[end_split:]
        test_id_list = id_list[start_split : end_split]
        # test_id_list = id_list[: int(len(id_list)*0.2)]            

         if not osp.exists('./data/AmurTigerMulti/flod' + str(i)):
            os.mkdir('./data/AmurTigerMulti/flod' + str(i))
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'bounding_box_train/')
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'bounding_box_test/')
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'query/')

            with open('./data/AmurTigerMulti/flod' + str(i) + '/' + 'map_trainpicname.txt', 'w') as f:
            #isi pacname.txt 001703.jpg mengacu ke 0_c6s2_1.jpg 0 adalah id,1 adalah urutan nomornya, 6 dan 2 adalah cam dari bilangan random
                kk = 0
                for id , file in tqdm(id_df.groupby('id')):
                    img_list = list(file['img_file'])
                    if id in train_id_list:
                        cam=0 #memberi nama kamera dari 0-jumlah image pada tiap id
                        for name in img_list:
                            #print(id,name)
                            #biar ada jarak di mapname.txt right align sebanyak 4 {:>4}
                            new_name = '{:>4}_c{}s{}_{}.jpg'.format(id,cam,cam,kk) #format id_camXshotX_kk
                            new_path = osp.join('./data/AmurTigerMulti/flod' + str(i) + '/' + 'bounding_box_train/', new_name)
                            shutil.copyfile(osp.join('./pr_data_multi/atrw_reid_train/train/', name), new_path)
                            f.write(name + ' ' + new_name + '\n')
                            kk += 1
                            cam+= 1 #memberi nama kamera dari 0-jumlah image pada tiap id
                f.close()
            with open('./data/AmurTigerMulti/flod' + str(i) + '/' + 'map_testpicname.txt', 'w') as f:
                 for id , file in tqdm(id_df.groupby('id')):
                    img_list = list(file['img_file'])    
                    if id in test_id_list:
                        cam=0 #memberi nama kamera dari 0-jumlah image pada tiap id
                        for name in img_list:
                            new_name = '{:>4}_c{}s{}_{}.jpg'.format(id,cam,cam,kk)
                            new_path_1 = osp.join('./data/AmurTigerMulti/flod' + str(i) + '/' + 'bounding_box_test/', new_name)
                            shutil.copyfile(osp.join('./pr_data_multi/atrw_reid_train/train/', name), new_path_1)
                            f.write(name + ' ' + new_name + '\n')
                            kk += 1
                            cam+= 1
                 f.close()