
import os
import os.path as opt
import pandas as pd
import json
import shutil
import cv2 as cv
from PIL import Image
import seaborn as sns
import warnings
import matplotlib.pyplot as plt
import numpy as np
from pycocotools.coco import COCO
import skimage.io as io
from tqdm import tqdm
from scipy.spatial.distance import pdist
import math
import time


#pose_ann_path = './pr_data_multi/atrw_anno_reid_train/coba.json'
pose_ann_path = './pr_data_multi/atrw_anno_reid_train/reid_keypoints_train.json'
img_path = './pr_data_multi/atrw_reid_train/train/'


print('*'*50)

skeleton = [[0, 2], [1, 2], [2, 14], [14, 5], [5, 6], [14, 3], [3, 4], [14, 13],
            [13, 7], [13, 10], [7, 8], [8, 9], [10, 11], [11, 12]]

def masked(img_path, x1, x2, x3, x4, y1, y2, y3, y4):
    img = cv.imread(img_path)
    (h, w, c) = img.shape
    mask = np.zeros((h, w, 3), np.uint8)                               #Define mask image size
    triangle = np.array([[x1, y1], [x2, y2], [x3, y3], [x4, y4]])       #Define the position of the mask part
    cv.fillConvexPoly(mask, triangle, (255, 255, 255))        #mask diberi putih pada bagian yang dipotong ingat putih nilai 255 artinya semua 1
    #plt.show() #kotak kuning         
    img_mask = cv.bitwise_and(img, mask)                                
    #plt.imshow(img_mask)
    #plt.show() #masked
   #Prevent out of bounds,jadi kotak deh
    xmin = min(x1, x2, x3, x4)
    xmax = max(x1, x2, x3, x4)
    ymin = min(y1, y2, y3, y4)
    ymax = max(y1, y2, y3, y4)

    if xmin <= 0:
        xmin = 0
    if ymin <= 0:
        ymin = 0
    if xmax > w:
        xmax = w
    if ymax > h:
        ymax = h
    if xmax <= 0 or ymax <=0 or xmin >= xmax or ymin >= ymax:
        return 1
    else:
        # print('name:{}, xmin:{},xmax:{}, ymin:{}, ymax:{}'.format(img_path, xmin, xmax, ymin, ymax))
        img_crop = img_mask[ymin: ymax, xmin: xmax]
        return img_crop
#According to the coordinates of the two points of the original paw, draw the frame to mark 
#ind is the category that distinguishes paws:
#1,2 is the front paw #3,5 are on the back paw #4, 6 are under the hind paws
def aabb_box(x1, y1, x2, y2, ind):
    L = math.sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2)) #panjang pythagoras

    tha = math.atan((x2 -x1)/((y2 -y1)+0.000000000001)) #hitung sudut dari gradien
    if ind == 1 or ind == 2:
        detalx = math.cos(tha) * 0.3 * L
        detaly = math.sin(tha) * 0.3 * L
    if ind == 3 or ind == 5:
        detalx = math.cos(tha) * 0.45 * L
        detaly = math.sin(tha) * 0.45 * L
    else:
        detalx = math.cos(tha) * 0.3 * L
        detaly = math.sin(tha) * 0.3 * L
   #Follow clockwise 1, 2, 3, 4 starting from the upper left corner
    xx1 = int(x1 - detalx)
    yy1 = int(y1 + detaly)
    xx2 = int(x1 + detalx)
    yy2 = int(y1 - detaly)
    xx4 = int(x2 - detalx)
    yy4 = int(y2 + detaly)
    xx3 = int(x2 + detalx)
    yy3 = int(y2 - detaly)

    return xx1, xx2, xx3, xx4, yy1, yy2, yy3, yy4


def remove0(x):
    while len(x) != 0:
        if 0 in x:
            x.remove(0)
        else:
            break
    return x

def show_ann_pic_bbox(name, pos):
    img = Image.open(opt.join(img_path, name))
    fig = plt.figure()
    ax = fig.add_subplot(111)
 
    
  
    skeleton = [[0, 2], [1, 2], [2, 14], [14, 5], [5, 6], [14, 3], [3, 4], [14, 13],
            [13, 7], [13, 10], [7, 8], [8, 9], [10, 11], [11, 12]]
    #Draw the connection of the joint points
    #print(pos[[2,14],0])
    #print(pos[[2,14],1]) 
    #print(pos[[2,14],2])
    for pair in skeleton:
        if np.all(pos[pair, 2] > 0): #Joint point visible
            #print(pos[[14],2])        
            color = (np.random.random((1, 3)) * 0.6 + 0.4).tolist()[0]
            plt.plot((pos[pair[0], 0], pos[pair[1], 0]), (pos[pair[0], 1], pos[pair[1], 1]), linewidth=3, color=color)
    #plt.show() #connection of the joint points(1)
    
    #Mark out joint points

    for i in range(15):
        if pos[i, 2] > 0:
            #print(pos[i,2])
            plt.plot(pos[i, 0], pos[i, 1], 'o', markersize=8, markeredgecolor='k', markerfacecolor='r')
            plt.annotate(i, # this is the text
                 (pos[i,0],pos[i,1]), # this is the point to label
                 textcoords="offset points", # how to position the text
                 xytext=(0,10), # distance from text to points (x,y)
                 ha='center') 
    plt.ylim(400, 0)
    #plt.show() #joint points (2) kalau (1) di comment gambarnya numpuk epic show 1
    #1.body:
    x1 = [pos[2, 0], pos[13, 0], pos[0, 0], pos[1, 0], pos[10, 0], pos[7, 0]]    
    x2 = [pos[2, 0], pos[13, 0], pos[0, 0], pos[1, 0], pos[10, 0], pos[7, 0]]   
    y1 = [pos[1, 1], pos[0, 1], pos[13, 1]]                                      
    y2 = [pos[10, 1], pos[7, 1], pos[3, 1], pos[5, 1]]                            
    #print(x1,x2,y1,y2) #dijadikan array untuk dilihat koordinat body terbaik untuk dipotong
   
    rect1 = plt.Rectangle((-200,-100), 400, 200, color='yellow')
  
    ax.add_patch(rect1)
    
   
    x1 = remove0(x1)
    x2 = remove0(x2)
    y1 = remove0(y1)
    y2 = remove0(y2)
    #print(x1,x2,y1,y2) #untuk data 0 perlu dihapus agar tidak menganggu min dan max saat mencari koordinat untuk memotong body
    if x1 and x2 and y1 and y2:      
        
         if len(x1) != 1:
             xmin = min(x1)
             xmax = max(x2)
             ymin = min(y1)
             ymax = max(y2)
             fig = plt.figure()
             ax = fig.add_subplot(111)
             rect = plt.Rectangle((xmin, ymin), xmax-xmin, ymax-ymin, edgecolor='yellow', fill=False, linewidth=2)
             ax.add_patch(rect)
             plt.xlim([0, 800])
             plt.ylim([400, 0])
             #plt.show() #kotak untuk potong body
            
             img_body = img.crop((xmin, ymin, xmax, ymax))
             img_body.save('./pr_data_multi/Pseudo_Mask/'+ name.split('.')[0] +'_body.jpg')
         else:
             pass
    else:
         pass

    # #2. paw
    pairs = [[5, 6], [3, 4], [7, 8], [8, 9], [10, 11], [11, 12]] 
    for ind, pair in enumerate(pairs):
        ind += 1
        #print(pair,ind)
        i, j = pair[0], pair[1]                                                 
        if 0 not in [pos[i, 2], pos[j, 2]]:                      
            x1 = pos[i, 0]
            y1 = pos[i, 1]
            x2 = pos[j, 0]
            y2 = pos[j, 1]
            xx1, xx2, xx3, xx4, yy1, yy2, yy3, yy4 = aabb_box(x1, y1, x2, y2, ind)
            plt.plot((xx1, xx2), (yy1, yy2), linewidth=3, color='yellow')
            plt.plot((xx2, xx3), (yy2, yy3), linewidth=3, color='yellow')
            plt.plot((xx3, xx4), (yy3, yy4), linewidth=3, color='yellow')
            plt.plot((xx4, xx1), (yy4, yy1), linewidth=3, color='yellow')
            #plt.show()  #kotak untuk potong paw

            img1 = masked(opt.join(img_path, name), xx1, xx2, xx3, xx4, yy1, yy2, yy3, yy4)
            if type(img1) == int:
                break
            else:
                img1 = Image.fromarray(cv.cvtColor(img1, cv.COLOR_BGR2RGB))            
                img1.save('./pr_data_multi/Pseudo_Mask/'+ name.split('.')[0] +'_'+ str(ind) +'part.jpg')
    
    #plt.show() #digabung semua kotak kuning

    plt.imshow(img)
    plt.axis('off')
    #plt.show() #epic show 2
    # plt.savefig(opt.join('data/split_by_id/pose/', name))
    plt.close('all')                               

def Second():
            #Semua di mask
        os.mkdir('./pr_data_multi/Pseudo_Mask/')
        with open(pose_ann_path, 'r') as f:
                pos_json = json.load(f)
                for i in tqdm(pos_json):
                    pic_name = i
                    #print(pic_name) nama gambar
                    pos = np.array(pos_json[i]).reshape((15, 3))
                    #print(pos) #isi dari json
                    show_ann_pic_bbox(pic_name, pos)

    #=======================================================================

        print('*'*25)
        for i in range(3):

            #TRAIN
            map_dic = {}
            with open('./data/AmurTigerMulti/flod' + str(i) + '/' + 'map_trainpicname.txt', 'r') as f:
                lines = f.readlines()
                for line in lines:
                    orign_name = line.split(' ')[0].split('.')[0]
                    new_name = line.split(' ')[-1].split('\n')[0].split('.')[0]
                    map_dic[orign_name] = new_name
            
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_Mask/')
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_Mask/'+'Body/')
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_Mask/'+'1part/')
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_Mask/'+'2part/')
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_Mask/'+'3part/')
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_Mask/'+'4part/')
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_Mask/'+'5part/')
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_Mask/'+'6part/')
            for pic in os.listdir('./pr_data_multi/Pseudo_Mask/'):
                name, part = pic.split('_')[0], pic.split('_')[1].split('.')[0]
                if map_dic.get(name):
                    #print('yes')
                    #print(part)
                    if part=='1part':
                        new_name = map_dic[name] + '.' + part + '.jpg'
                        shutil.copyfile('./pr_data_multi/Pseudo_Mask/' + pic, './data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_Mask/' +'1part/'+ new_name)
                    
                    if part=='2part':
                        new_name = map_dic[name] + '.' + part + '.jpg'
                        shutil.copyfile('./pr_data_multi/Pseudo_Mask/' + pic, './data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_Mask/' +'2part/'+ new_name)

                    if part=='3part':
                        new_name = map_dic[name] + '.' + part + '.jpg'
                        shutil.copyfile('./pr_data_multi/Pseudo_Mask/' + pic, './data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_Mask/' +'3part/'+ new_name)
            
                    if part=='4part':
                        new_name = map_dic[name] + '.' + part + '.jpg'
                        shutil.copyfile('./pr_data_multi/Pseudo_Mask/' + pic, './data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_Mask/' +'4part/'+ new_name)
            
                    if part=='5part':
                        new_name = map_dic[name] + '.' + part + '.jpg'
                        shutil.copyfile('./pr_data_multi/Pseudo_Mask/' + pic, './data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_Mask/' +'5part/'+ new_name)
                    
                    if part=='6part':
                        new_name = map_dic[name] + '.' + part + '.jpg'
                        shutil.copyfile('./pr_data_multi/Pseudo_Mask/' + pic, './data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_Mask/' +'6part/'+ new_name)
                    

                    if part=='body':
                        new_name = map_dic[name] + '.' + part + '.jpg'
                        shutil.copyfile('./pr_data_multi/Pseudo_Mask/' + pic, './data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_Mask/' +'Body/'+ new_name)
            
            
            #TEST
            map_dic = {}
            with open('./data/AmurTigerMulti/flod' + str(i) + '/' + 'map_testpicname.txt', 'r') as f:
                lines = f.readlines()
                for line in lines:
                    orign_name = line.split(' ')[0].split('.')[0]
                    new_name = line.split(' ')[-1].split('\n')[0].split('.')[0]
                    map_dic[orign_name] = new_name
            
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_MaskTEST/')
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_MaskTEST/'+'Body/')
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_MaskTEST/'+'1part/')
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_MaskTEST/'+'2part/')
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_MaskTEST/'+'3part/')
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_MaskTEST/'+'4part/')
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_MaskTEST/'+'5part/')
            os.mkdir('./data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_MaskTEST/'+'6part/')
            for pic in os.listdir('./pr_data_multi/Pseudo_Mask/'):
                name, part = pic.split('_')[0], pic.split('_')[1].split('.')[0]
                if map_dic.get(name):
                    #print('yes')
                    #print(part)
                    if part=='1part':
                        new_name = map_dic[name] + '.' + part + '.jpg'
                        shutil.copyfile('./pr_data_multi/Pseudo_Mask/' + pic, './data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_MaskTEST/' +'1part/'+ new_name)
                    
                    if part=='2part':
                        new_name = map_dic[name] + '.' + part + '.jpg'
                        shutil.copyfile('./pr_data_multi/Pseudo_Mask/' + pic, './data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_MaskTEST/' +'2part/'+ new_name)

                    if part=='3part':
                        new_name = map_dic[name] + '.' + part + '.jpg'
                        shutil.copyfile('./pr_data_multi/Pseudo_Mask/' + pic, './data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_MaskTEST/' +'3part/'+ new_name)
            
                    if part=='4part':
                        new_name = map_dic[name] + '.' + part + '.jpg'
                        shutil.copyfile('./pr_data_multi/Pseudo_Mask/' + pic, './data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_MaskTEST/' +'4part/'+ new_name)
            
                    if part=='5part':
                        new_name = map_dic[name] + '.' + part + '.jpg'
                        shutil.copyfile('./pr_data_multi/Pseudo_Mask/' + pic, './data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_MaskTEST/' +'5part/'+ new_name)
                    
                    if part=='6part':
                        new_name = map_dic[name] + '.' + part + '.jpg'
                        shutil.copyfile('./pr_data_multi/Pseudo_Mask/' + pic, './data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_MaskTEST/' +'6part/'+ new_name)
            
                    if part=='body':
                        new_name = map_dic[name] + '.' + part + '.jpg'
                        shutil.copyfile('./pr_data_multi/Pseudo_Mask/' + pic, './data/AmurTigerMulti/flod' + str(i) + '/' + 'Pseudo_MaskTEST/' +'Body/'+ new_name)
            
            