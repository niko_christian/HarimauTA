import copy
import torch
import torch.nn as nn
from torchvision.models.resnet import resnet50, Bottleneck

num_classes = 77  # change this depend on your dataset


class CVWC(nn.Module):
    def __init__(self):
        super(CVWC, self).__init__()

        feats = 256
        resnet = resnet50(pretrained=True)

        self.backbone = nn.Sequential(
            resnet.conv1,
            resnet.bn1,
            resnet.relu,
            resnet.maxpool,
            resnet.layer1,
            resnet.layer2,
            resnet.layer3[0],
        )

        res_conv4 = nn.Sequential(*resnet.layer3[1:])

        res_g_conv5 = resnet.layer4

        res_p_conv5 = nn.Sequential(
            Bottleneck(1024, 512, downsample=nn.Sequential(nn.Conv2d(1024, 2048, 1, bias=False), nn.BatchNorm2d(2048))),
            Bottleneck(2048, 512),
            Bottleneck(2048, 512))
        res_p_conv5.load_state_dict(resnet.layer4.state_dict())

        self.g = nn.Sequential(copy.deepcopy(res_conv4), copy.deepcopy(res_g_conv5))
        self.b = nn.Sequential(copy.deepcopy(res_conv4), copy.deepcopy(res_g_conv5))
        self.p1 = nn.Sequential(copy.deepcopy(res_conv4), copy.deepcopy(res_p_conv5))
        self.p2 = nn.Sequential(copy.deepcopy(res_conv4), copy.deepcopy(res_p_conv5))
        self.p3 = nn.Sequential(copy.deepcopy(res_conv4), copy.deepcopy(res_p_conv5))
        self.p4 = nn.Sequential(copy.deepcopy(res_conv4), copy.deepcopy(res_p_conv5))
        self.p5 = nn.Sequential(copy.deepcopy(res_conv4), copy.deepcopy(res_p_conv5))
        self.p6 = nn.Sequential(copy.deepcopy(res_conv4), copy.deepcopy(res_p_conv5))

        self.maxpool_zg_g = nn.MaxPool2d(kernel_size=(8, 18))
        self.maxpool_zp_b = nn.MaxPool2d(kernel_size=(8, 9))
        self.maxpool_zp_p = nn.MaxPool2d(kernel_size=(8, 5))

        self.reduction = nn.Sequential(nn.Conv2d(2048, feats, 1, bias=False), nn.BatchNorm2d(feats), nn.ReLU())

        self._init_reduction(self.reduction)

        self.fc_id_2048_0 = nn.Linear(feats, num_classes)
        self.fc_id_2048_1 = nn.Linear(feats, num_classes)

        self.fc_id_256_1 = nn.Linear(feats, num_classes)
        self.fc_id_256_2 = nn.Linear(feats, num_classes)
        self.fc_id_256_3 = nn.Linear(feats, num_classes)
        self.fc_id_256_4 = nn.Linear(feats, num_classes)
        self.fc_id_256_5 = nn.Linear(feats, num_classes)
        self.fc_id_256_6 = nn.Linear(feats, num_classes)

        self._init_fc(self.fc_id_2048_0)
        self._init_fc(self.fc_id_2048_1)
       
        self._init_fc(self.fc_id_256_1)
        self._init_fc(self.fc_id_256_2)
        self._init_fc(self.fc_id_256_3)
        self._init_fc(self.fc_id_256_4)
        self._init_fc(self.fc_id_256_5)
        self._init_fc(self.fc_id_256_6)
        

    @staticmethod
    def _init_reduction(reduction):
        # conv
        nn.init.kaiming_normal_(reduction[0].weight, mode='fan_in')
        # nn.init.constant_(reduction[0].bias, 0.)

        # bn
        nn.init.normal_(reduction[1].weight, mean=1., std=0.02)
        nn.init.constant_(reduction[1].bias, 0.)

    @staticmethod
    def _init_fc(fc):
        nn.init.kaiming_normal_(fc.weight, mode='fan_out')
        # nn.init.normal_(fc.weight, std=0.001)
        nn.init.constant_(fc.bias, 0.)

    def forward(self, x):
        x = self.backbone(x)
        # b = self.backbone(x)
        # p1 = self.backbone(x)
        # p2 = self.backbone(x)
        # p3 = self.backbone(x)
        # p4 = self.backbone(x)
        # p5 = self.backbone(x)
        # p6 = self.backbone(x)

        g = self.g(x)
        # b = self.b(x)
        # # print('b',b.size())
        # p1 = self.p1(x)
        # p2 = self.p2(x)
        # p3 = self.p3(x)
        # p4 = self.p4(x)
        # p5 = self.p5(x)
        # p6 = self.p6(x)

        # print(p1.size())
        # print(p2.size())
        # print(p3.size())
        # print(p4.size())
        # print(p5.size())
        # print(p6.size())
        #p2 = self.p2(x)
        #p3 = self.p3(x)

        zg_g = self.maxpool_zg_g(g)
        # zg_b = self.maxpool_zp_g(b)
        # zg_p1 = self.maxpool_zp_g(p1)
        # zg_p2 = self.maxpool_zp_g(p2)
        # zg_p3 = self.maxpool_zp_g(p3)
        # zg_p4 = self.maxpool_zp_g(p4)
        # zg_p5 = self.maxpool_zp_g(p5)
        # zg_p6 = self.maxpool_zp_g(p6)
    

        fg_g = self.reduction(zg_g).squeeze(dim=3).squeeze(dim=2)
        # fg_b = self.reduction(zg_b).squeeze(dim=3).squeeze(dim=2)
        # fg_p1 = self.reduction(zg_p1).squeeze(dim=3).squeeze(dim=2)
        # fg_p2 = self.reduction(zg_p2).squeeze(dim=3).squeeze(dim=2)
        # fg_p3 = self.reduction(zg_p3).squeeze(dim=3).squeeze(dim=2)
        # fg_p4 = self.reduction(zg_p4).squeeze(dim=3).squeeze(dim=2)
        # fg_p5 = self.reduction(zg_p5).squeeze(dim=3).squeeze(dim=2)
        # fg_p6 = self.reduction(zg_p6).squeeze(dim=3).squeeze(dim=2)
        

        l_g = self.fc_id_2048_0(fg_g)

        # l_b = self.fc_id_2048_1(fg_b)

        # l_p1 = self.fc_id_256_1(fg_p1)
        # l_p2 = self.fc_id_256_2(fg_p2)
        # l_p3 = self.fc_id_256_3(fg_p3)
        # l_p4 = self.fc_id_256_4(fg_p4)
        # l_p5 = self.fc_id_256_5(fg_p5)
        # l_p6 = self.fc_id_256_6(fg_p6)

        predict = torch.cat([fg_g], dim=1)

       
        return predict,fg_g,l_g