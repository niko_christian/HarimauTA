

import os
import os.path as osp
import pandas as pd
import random
import shutil
from tqdm import tqdm

def RENAME():
    id_path = './pr_data/atrw_anno_reid_train/reid_list_train.csv'
    id_df = pd.read_csv(id_path, header=None, names=['id', 'img_file'])
    id_list = list(id_df['id'].unique())
    print(id_list)
    random.shuffle(id_list) #random                     
    #split_num = int(len(id_list)*0.2) 
    split_num=30
    print('split_num',split_num)          

    for i in range(3): #cross validation
        start_split = i * split_num
        end_split = (i + 1) * split_num
        print('start_split',start_split)  
        print('end_split',end_split)          
        train_id_list = id_list[:start_split] + id_list[end_split:]
        print(len(train_id_list))
        print(train_id_list)
        test_id_list = id_list[start_split : end_split]
        print(len(test_id_list))
        print(test_id_list)
            
        if not osp.exists('./data/AmurTiger/flod' + str(i)):
            os.mkdir('./data/AmurTiger/flod' + str(i))
            os.mkdir('./data/AmurTiger/flod' + str(i) + '/' + 'bounding_box_train/')
            os.mkdir('./data/AmurTiger/flod' + str(i) + '/' + 'bounding_box_test/')
            os.mkdir('./data/AmurTiger/flod' + str(i) + '/' + 'query/')

            with open('./data/AmurTiger/flod' + str(i) + '/' + 'map_trainpicname.txt', 'w') as f:
            #isi pacname.txt 001703.jpg mengacu ke 0_c6s2_1.jpg 0 adalah id,1 adalah urutan nomornya, 6 dan 2 adalah cam dari bilangan random
                kk = 0
                for id , file in tqdm(id_df.groupby('id')):
                    img_list = list(file['img_file'])
                    if id in train_id_list:
                        cam=0 #memberi nama kamera dari 0-jumlah image pada tiap id
                        for name in img_list:
                            #print(id,name)
                            #biar ada jarak di mapname.txt right align sebanyak 4 {:>4}
                            new_name = '{:>4}_c{}s{}_{}.jpg'.format(id,cam,cam,kk) #format id_camXshotX_kk
                            new_path = osp.join('./data/AmurTiger/flod' + str(i) + '/' + 'bounding_box_train/', new_name)
                            shutil.copyfile(osp.join('./pr_data/atrw_reid_train/train/', name), new_path)
                            f.write(name + ' ' + new_name + '\n')
                            kk += 1
                            cam+= 1 #memberi nama kamera dari 0-jumlah image pada tiap id
                f.close()
            with open('./data/AmurTiger/flod' + str(i) + '/' + 'map_testpicname.txt', 'w') as f:
                 for id , file in tqdm(id_df.groupby('id')):
                    img_list = list(file['img_file'])    
                    if id in test_id_list:
                        cam=0 #memberi nama kamera dari 0-jumlah image pada tiap id
                        for name in img_list:
                            new_name = '{:>4}_c{}s{}_{}.jpg'.format(id,cam,cam,kk)
                            new_path_1 = osp.join('./data/AmurTiger/flod' + str(i) + '/' + 'bounding_box_test/', new_name)
                            shutil.copyfile(osp.join('./pr_data/atrw_reid_train/train/', name), new_path_1)
                            f.write(name + ' ' + new_name + '\n')
                            kk += 1
                            cam+= 1
                 f.close()

