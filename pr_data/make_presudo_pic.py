

import os
import os.path as osp
import shutil
from PIL import Image
from tqdm import tqdm

path = './data/AmurTiger/'

to_do_list = ['bounding_box_train','bounding_box_test']

def FLIP():
    for i in range(3):
        for n in to_do_list:
            for pic in tqdm(os.listdir(path + 'flod' + str(i) + '/' + n)):
                im = Image.open(path + 'flod' + str(i) + '/' + n + '/' + pic)
                new_im = im.transpose(Image.FLIP_LEFT_RIGHT)
                name, ext = pic.split('.')   
                new_im.save(path + 'flod' + str(i) + '/' + 'query' + '/' + name+ "_999.jpg") #yang flip diberi angka 999
                # if(n!='bounding_box_test'):
        #         #     im.save(path + 'flod' + str(i) + '/' + n + '_filp' + '/' + pic) #buat simpen yang gk flip
        # os.rename(path + 'flod' + str(i) + '/' +'bounding_box_test'+ '_filp',path + 'flod' + str(i) + '/' +'query')

