import copy
import torch
import torch.nn as nn
from torchvision.models.resnet import resnet34, Bottleneck

num_classes = 107  # change this depend on your dataset


class CVWC34(nn.Module):
    def __init__(self):
        super(CVWC34, self).__init__()

        feats = 512
        resnet = resnet34(pretrained=True)
        

        self.backbone = nn.Sequential(
            resnet.conv1,
            resnet.bn1,
            resnet.relu,
            resnet.maxpool,
            resnet.layer1,
            resnet.layer2,
            resnet.layer3,
            resnet.layer4
        )


        #self.maxpool_zg_p1 = nn.MaxPool2d(kernel_size=(8, 18))
        self.maxpool_zg_p1 = nn.AdaptiveAvgPool2d((1,1))


        self.reduction = nn.Sequential(nn.Conv2d(512, feats, 1, bias=False), nn.BatchNorm2d(feats), nn.ReLU())

        self._init_reduction(self.reduction)

        self.fc_id_2048_0 = nn.Linear(feats, num_classes)

        self._init_fc(self.fc_id_2048_0)
        

    @staticmethod
    def _init_reduction(reduction):
        # conv
        nn.init.kaiming_normal_(reduction[0].weight, mode='fan_in')
        # nn.init.constant_(reduction[0].bias, 0.)

        # bn
        nn.init.normal_(reduction[1].weight, mean=1., std=0.02)
        nn.init.constant_(reduction[1].bias, 0.)

    @staticmethod
    def _init_fc(fc):
        nn.init.kaiming_normal_(fc.weight, mode='fan_out')
        # nn.init.normal_(fc.weight, std=0.001)
        nn.init.constant_(fc.bias, 0.)

    def forward(self, x):
        x = self.backbone(x)
       

        fg_p1 = self.maxpool_zg_p1(x).squeeze(dim=3).squeeze(dim=2)

        #print(zg_p1.size())
       
        #fg_p1 = self.reduction(zg_p1)

        #print(fg_p1.size())
       
        l_p1 = self.fc_id_2048_0(fg_p1)
       
        predict = torch.cat([fg_p1], dim=1)

        return predict, fg_p1, l_p1