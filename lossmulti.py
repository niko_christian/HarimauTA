from torch.nn import CrossEntropyLoss
from torch.nn.modules import loss
from utils.TripletLoss import TripletLoss


class Loss(loss._Loss):
    def __init__(self):
        super(Loss, self).__init__()

    def forward(self, outputs, labels):
        cross_entropy_loss = CrossEntropyLoss()
        triplet_loss = TripletLoss(margin=1.2)

        # Triplet_LossBody = [triplet_loss(output, labels) for output in outputs[1:3]]
        # Triplet_LossBody = sum(Triplet_LossBody) / len(Triplet_LossBody)

        # Triplet_LossPart = [triplet_loss(output, labels) for output in outputs[3:9]]
        # Triplet_LossPart = sum(Triplet_LossPart) / len(Triplet_LossPart)

        # Triplet_Loss=(2 *Triplet_LossBody+Triplet_LossPart)/3

        Triplet_Loss = [triplet_loss(output, labels) for output in outputs[1:9]]
        Triplet_Loss = sum(Triplet_Loss) / len(Triplet_Loss)

        CrossEntropy_Loss = [cross_entropy_loss(output, labels) for output in outputs[9:]]
        CrossEntropy_Loss = sum(CrossEntropy_Loss) / len(CrossEntropy_Loss)

        loss_sum = Triplet_Loss + 2 * CrossEntropy_Loss

      
        print('\rtotal loss:%.2f  Triplet_Loss:%.2f  CrossEntropy_Loss:%.2f' % (
            loss_sum.data.cpu().numpy(),
            Triplet_Loss.data.cpu().numpy(),
            CrossEntropy_Loss.data.cpu().numpy()),
              end=' ')
        # print('\r Triplet_Loss:%.2f  ' % (
        #     #loss_sum.data.cpu().numpy(),
        #     Triplet_Loss.data.cpu().numpy()),
        #    # CrossEntropy_Loss.data.cpu().numpy()),
        #       end=' ')
        return loss_sum
