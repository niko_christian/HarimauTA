import torch


def extract_feature(model, loader):
    features = torch.FloatTensor()

    for (inputs, labels) in loader:

        input_img = inputs.to('cuda')
        outputs = model(input_img)
       
        f1 = outputs[0].data.cpu()

        # flip
        # inputs = inputs.index_select(3, torch.arange(inputs.size(3) - 1, -1, -1))
        # input_img = inputs.to('cuda')
        # outputs = model(input_img)
        # f2 = outputs[0].data.cpu()
        # ff = f1 + f2

        fnorm = torch.norm(f1, p=2, dim=1, keepdim=True)
        f1 = f1.div(fnorm.expand_as(f1))
        features = torch.cat((features, f1), 0)
    return features