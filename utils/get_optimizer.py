from torch.optim import Adam, SGD
from opt import opt


def get_optimizer(net):
  
        if (opt.optimizer=='adam'):
          optimizer = Adam(net.parameters(), lr=opt.lr, weight_decay=5e-4, amsgrad=True)
        if (opt.optimizer=='SGD'):
          optimizer = SGD(net.parameters(), lr=opt.lr,momentum=0.9, weight_decay=5e-4,nesterov=False)
        
        return optimizer
