import random
import collections
from torch.utils.data import sampler


class RandomSampler(sampler.Sampler):
    def __init__(self, data_source, batch_id, batch_image):
        super(RandomSampler, self).__init__(data_source)

        self.data_source = data_source
        self.batch_image = batch_image
        self.batch_id = batch_id

        self._id2index = collections.defaultdict(list) #membuat list kosong

        for idx, path in enumerate(data_source.imgs):
            _id = data_source.id(path) #mengambil id dari fungsi id dari data.py
            self._id2index[_id].append(idx)
        #print(self._id2index) #berisi kamus label ada pada id mana 2(id):[0,1,2](label)
        #self.__iter__()

    def __iter__(self):
        unique_ids = self.data_source.unique_ids
        random.shuffle(unique_ids) #mengacak unique id

        imgs = []
        
        i=0
        for _id in unique_ids:
            imgs.extend(self._sample(self._id2index[_id], self.batch_image)) #batch image= image yang diambil per ID
            i=i+1
            # if(i<=2):
              #print(imgs)
        
        return iter(imgs)

    def __len__(self):
        return len(self._id2index) * self.batch_image

    @staticmethod
    def _sample(population, k):
        if len(population) < k:
            population = population * k
        return random.sample(population, k)
