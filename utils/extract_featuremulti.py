import torch


def extract_feature(model, loader):
    features = torch.FloatTensor()
  
    # for batch, () in enumerate(self.train_loader):
            
    for (inputs,body,part1,part2,part3,part4,part5,part6,labels) in loader:

        inputs = inputs.to('cuda')
        labels = labels.to('cuda')
        body   = body.to('cuda')
        part1  = part1.to('cuda')
        part2  = part2.to('cuda')
        part3  = part3.to('cuda')
        part4  = part4.to('cuda')
        part5  = part5.to('cuda')
        part6  = part6.to('cuda')
        outputs = model(inputs,body,part1,part2,part3,part4,part5,part6)
      
        f1 = outputs[0].data.cpu()

        # flip
        # inputs = inputs.index_select(3, torch.arange(inputs.size(3) - 1, -1, -1))
        # input_img = inputs.to('cuda')
        # outputs = model(input_img)
        # f2 = outputs[0].data.cpu()
        # ff = f1 + f2

        fnorm = torch.norm(f1, p=2, dim=1, keepdim=True)
        f1 = f1.div(fnorm.expand_as(f1))
        features = torch.cat((features, f1), 0)
    return features
