from torchvision import transforms
from torch.utils.data import dataset, dataloader
from torchvision.datasets.folder import default_loader
from utils.RandomErasing import RandomErasing
from utils.RandomSampler import RandomSampler
from optmulti import opt
import os
import re


class Data():
    def __init__(self):
        train_transform = transforms.Compose([
            transforms.Resize((256, 576), interpolation=3), #(heigth,width)
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
            RandomErasing(probability=0.5, mean=[0.0, 0.0, 0.0])
        ])

        test_transform = transforms.Compose([
            transforms.Resize((256, 576), interpolation=3),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])


        body_transform = transforms.Compose([
            transforms.Resize((256, 288), interpolation=3),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])

        part_transform = transforms.Compose([
            transforms.Resize((128, 72), interpolation=3),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])

        self.trainset = Market1501(train_transform, 'train', opt.data_path)
        self.testset = Market1501(test_transform, 'query', opt.data_path)
        self.queryset = Market1501(test_transform, 'test', opt.data_path)

        self.train_loader = dataloader.DataLoader(self.trainset,
                                                  sampler=RandomSampler(self.trainset, batch_id=opt.batchid,
                                                                        batch_image=opt.batchimage),
                                                  batch_size=opt.batchid * opt.batchimage, num_workers=2,
                                                  pin_memory=True) #berapa banyak id dalam sekali training

        self.test_loader = dataloader.DataLoader(self.testset, batch_size=opt.batchtest, num_workers=2, pin_memory=True)
        self.query_loader = dataloader.DataLoader(self.queryset, batch_size=opt.batchtest, num_workers=2,
                                                  pin_memory=True)

        # if opt.mode == 'vis':
        #     img = test_transform(default_loader(opt.query_image))
        #     bodyimg = body_transform(default_loader(opt.querybody_image))
        #     part1img = part_transform(default_loader(opt.querypart1_image))
        #     part2img = part_transform(default_loader(opt.querypart2_image))
        #     part3img = part_transform(default_loader(opt.querypart3_image))
        #     part4img = part_transform(default_loader(opt.querypart4_image))
        #     part5img = part_transform(default_loader(opt.querypart5_image))
        #     part6img = part_transform(default_loader(opt.querypart6_image))
        
        #return img,bodyimg,part1img,part2img,part3img,part4img,part5img,part6img,target






class Market1501(dataset.Dataset):
    def __init__(self, transform, dtype, data_path):

        self.transform = transform
        self.loader = default_loader
        self.data_path = data_path

        if dtype == 'train':
            self.data_path += '/bounding_box_train'
        elif  opt.mode == 'vis' and  dtype == 'test':
            self.data_path = '/content/HarimauTA/QueryVis'
        elif dtype == 'test':
            self.data_path += '/bounding_box_test'
        else:
            self.data_path += '/query'
            

        self.imgs = [path for path in self.list_pictures(self.data_path) if self.id(path) != -1]
        #print('list gambar',self.imgs)
        self._id2label = {_id: idx for idx, _id in enumerate(self.unique_ids)}
        # print('unique id',self.unique_ids)
        # print('camera',self.cameras)
        # print('ID',self.ids) #dari ID 
        # print('ID2label',self._id2label) #ID diberi label [1(id):0(label),3(id):1(label)] 
        # print(self.__len__())
        # print(self.__getitem__(1))
        

    def __getitem__(self, index):
        path = self.imgs[index]
        
        #print(index,end=" ")
        #print(path) #dari path nanti bisa diarahkan ke yang lain
        target = self._id2label[self.id(path)]
        #print(target)
        x = path.split("/")
        I='0';
        if(len(x)>5):
          flodi=x[5]
          I=flodi[4]
        
        imgnameExt = x[len(x) - 1]
        dtype= x[len(x) - 2]
        imgname=imgnameExt.split('.')
        hapustab=imgname[0].strip()
        body =hapustab+".body"+".jpg"
        part1=hapustab+".1part"+".jpg"
        part2=hapustab+".2part"+".jpg"
        part3=hapustab+".3part"+".jpg"
        part4=hapustab+".4part"+".jpg"
        part5=hapustab+".5part"+".jpg"
        part6=hapustab+".6part"+".jpg"

        partpath="/content/HarimauTA/QueryVis/"
      
        if(dtype=='bounding_box_train'):
          partpath="/content/HarimauTA/data/AmurTigerMulti/flod"+I+"/Pseudo_Mask/"
        elif(dtype=='bounding_box_test'):
          partpath="/content/HarimauTA/data/AmurTigerMulti/flod"+I+"/Pseudo_MaskTEST/"
        if(dtype=='query'):
          partpath="/content/HarimauTA/data/AmurTigerMulti/flod"+I+"/PseudoMaskQuery/"
        
      
        bodypath=partpath+"Body/"+body
        part1path=partpath+"1part/"+part1
        part2path=partpath+"2part/"+part2
        part3path=partpath+"3part/"+part3
        part4path=partpath+"4part/"+part4
        part5path=partpath+"5part/"+part5
        part6path=partpath+"6part/"+part6

        #print(bodypath)
        
        #print(part1path)    
        #print(path)  
        
        bodyimg=self.loader(bodypath)
        part1img=self.loader(part1path)
        part2img=self.loader(part2path)
        part3img=self.loader(part3path)
        part4img=self.loader(part4path)
        part5img=self.loader(part5path)
        part6img=self.loader(part6path)

        body_transform = transforms.Compose([
            transforms.Resize((256, 288), interpolation=3),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])

        part_transform = transforms.Compose([
            transforms.Resize((128, 72), interpolation=3),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])
        
        #print(body)
        img = self.loader(path)
        #img2 = self.loader(path)
        if self.transform is not None:
            img = self.transform(img)
        

        bodyimg=body_transform(bodyimg)
        part1img=part_transform(part1img)
        part2img=part_transform(part2img)
        part3img=part_transform(part3img)
        part4img=part_transform(part4img)
        part5img=part_transform(part5img)
        part6img=part_transform(part6img)

        # if self.transform is not None:
        #     img2 = self.transform(img2)
            
        #return img, target #img2

        return img,bodyimg,part1img,part2img,part3img,part4img,part5img,part6img,target

    def __len__(self):
        return len(self.imgs)

   
    @staticmethod
    def id(file_path):
        """
        :param file_path: unix style file path
        :return: person id
        """
        return int(file_path.split('/')[-1].split('_')[0])

    @staticmethod
    def camera(file_path):
        """
        :param file_path: unix style file path
        :return: camera id
        """
        return int(file_path.split('/')[-1].split('_')[1][1])

    @property
    def ids(self):
        """
        :return: person id list corresponding to dataset image paths
        """
        return [self.id(path) for path in self.imgs]

    @property
    def unique_ids(self):
        """
        :return: unique person ids in ascending order
        """
        return sorted(set(self.ids))

    @property
    def cameras(self):
        """
        :return: camera id list corresponding to dataset image paths
        """
        return [self.camera(path) for path in self.imgs]

    @staticmethod
    def list_pictures(directory, ext='jpg|jpeg|bmp|png|ppm|npy'):
        assert os.path.isdir(directory), 'dataset is not exists!{}'.format(directory)

        return sorted([os.path.join(root, f)
                       for root, _, files in os.walk(directory) for f in files
                       if re.match(r'([\w]+\.(?:' + ext + '))', f.strip())])

#Data()