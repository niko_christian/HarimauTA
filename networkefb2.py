import copy
import torch
import torch.nn as nn
from efficientnet_pytorch import EfficientNet


num_classes = 107  # change this depend on your dataset


class EFB2(nn.Module):
    def __init__(self):
        super(EFB2, self).__init__()

        feats = 256
        self.model = EfficientNet.from_pretrained('efficientnet-b2')
       
        self.classifier_layer = nn.Sequential(
            nn.Linear(1280 , 512),
            nn.BatchNorm1d(512),
            nn.Dropout(0.2),
            nn.Linear(512 , 256)
        )
        self.fc_id = nn.Linear(1408, num_classes)

        self._init_fc(self.fc_id)
    
    @staticmethod
    def _init_fc(fc):
        nn.init.kaiming_normal_(fc.weight, mode='fan_out')
        # nn.init.normal_(fc.weight, std=0.001)
        nn.init.constant_(fc.bias, 0.)
        

    def forward(self, inputs):
        x = self.model.extract_features(inputs)
        x = self.model._avg_pooling(x)
        x = x.flatten(start_dim=1)
        fg_p1 = self.model._dropout(x)        
        #fg_p1 = self.classifier_layer(x)
        l_p1  = self.fc_id(fg_p1)
       
        predict = torch.cat([fg_p1], dim=1)

        return predict, fg_p1, l_p1